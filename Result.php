<?php
$answer = array(
    1 => "C",
    2 => "D",
    3 => "B",
    4 => "B",
    5 => "A",
    6 => "C",
    7 => "D",
    8 => "C",
    9 => "D",
    10 => "A"
);

$score = 0;
if (isset($_COOKIE["cau1"])) {
    for ($i = 0; $i < 10; $i++) {
        # Check question isn't done.
        if (key_exists("cau". $i + 1 ."", $_COOKIE) == true) {
            if ($_COOKIE["cau". $i + 1 .""] == $answer[$i+1]) {
                $score ++;
            }
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Result</title>
    <link rel="stylesheet" href="ThemeResult.css">
</head>
<body>
    <div class="wrapper">
        <div class="info">
            <?php
                if ($score < 4) {
                    echo "
                        <label class=\"score\">$score Điểm</label> <br>
                        <label class=\"content\">Bạn quá kém cần ôn tập thêm</label>
                    ";
                } else if ($score > 4 && $score < 7){
                    echo "
                        <label class=\"score\">$score Điểm</label> <br>
                        <label class=\"content\">Cũng bình thường</label>
                    ";   
                } else {
                    echo "
                        <label class=\"score\">$score Điểm</label> <br>
                        <label class=\"content\">Sắp sửa làm trợ giảng lớp PHP</label>
                    ";
                }
            ?> 
        </div>
    
    </div>
</body>
</html>