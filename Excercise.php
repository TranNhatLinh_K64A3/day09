<?php


$question = array(
    1 => "PHP tượng trưng cho cái gì:",
    2 => "Đoạn mã nào sau đây được sử dụng để chú thích PHP",
    3 => "Mặc định của một biến không có giá trị được thể hiện với từ khóa",
    4 => "Ký hiệu nào được dùng khi sử dụng biến trong PHP",
    5 => "Câu này cho điểm đáp án là A",
    6 => "Hàm nào sau đây dùng để khai báo hằng số",
    7 => "Đâu không phải là phép toán được dùng so sánh trong PHP",
    8 => "Giá trị của tham số sau: \$var = 1 / 2;",
    9 => "Chẵn hay lẻ ?",
    10 => "Tài hay xỉu ?"
);

$four_answer = array(
    1 => array(
        "a" => "Preprocessed Hypertext Page",
        "b" => "Hypertext Transfer Protocol",
        "c" => "PHP: Hypertext Preprocessor",
        "d" => "Hypertext Markup Language"
    ), 
    2 => array(
        "a" => "/* commented code here */",
        "b" => "// you are handsome",
        "c" => "# you are gay",
        "d" => "Tất cả các ý trên"
    ), 
    3 => array(
        "a" => "none",
        "b" => "null",
        "c" => "undef",
        "d" => "Không có khái niệm như vậy trong PHP"
    ), 
    4 => array(
        "a" => "$$",
        "b" => "$",
        "c" => "@",
        "d" => "#"
    ), 
    5 => array(
        "a" => "A",
        "b" => "B",
        "c" => "C",
        "d" => "D"
    ), 
    6 => array(
        "a" => "const",
        "b" => "constants",
        "c" => "define",
        "d" => "def"
    ), 
    7 => array(
        "a" => "===",
        "b" => ">=",
        "c" => "!=",
        "d" => "<=>"
    ), 
    8 => array(
        "a" => "1",
        "b" => "0",
        "c" => "0.5",
        "d" => "1/2"
    ),
    9 => array(
        "a" => "Lẻ",
        "b" => "Chẵn",
        "c" => "Cả 2",
        "d" => "Là Chẵn"
    ), 
    10 => array(
        "a" => "Tài",
        "b" => "Xỉu",
        "c" => "Học tài thi xỉu",
        "d" => "Xỉu up xỉu down"
    ),  
);



for ($i = 0; $i < 10; $i++) {
    if (isset($_POST["cau". $i + 1 .""])) {
        setcookie("cau". $i + 1 ."", $_POST["cau". $i + 1 .""], time() + 60, "/");
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    header("Refresh:0; url=Result.php");
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Excercise</title>
    <link rel="stylesheet" href="ThemeExcercise.css">
</head>

<body>
    <div class="wrapper">
        <form action="" method="POST">
            <div class="question-1" id="ques1">
                <?php
                    for ($i = 1; $i <= 5; $i++) {
                        echo "
                        <div class=\"ex\">
                            <label class=\"ques\" >Câu $i: ". $question[$i] ." </label> <br>
                            <input type=\"radio\" name=\"cau$i\" value=\"A\" >". $four_answer[$i]["a"] ." <br>
                            <input type=\"radio\" name=\"cau$i\" value=\"B\" >". $four_answer[$i]["b"] ."<br>
                            <input type=\"radio\" name=\"cau$i\" value=\"C\" >". $four_answer[$i]["c"] ."<br>
                            <input type=\"radio\" name=\"cau$i\" value=\"D\" >". $four_answer[$i]["d"] ."<br>
                            <br>
                        </div>
                        ";
                    }
                ?>
            </div>
            <div class="question-2" id="ques2">
                <?php
                    for ($i = 6; $i <= 10; $i++) {
                        echo "
                        <div class=\"ex\">
                            <label class=\"ques\">Câu $i: ". $question[$i] ." </label> <br>
                            <input type=\"radio\" name=\"cau$i\" value=\"A\" >". $four_answer[$i]["a"] ."<br>
                            <input type=\"radio\" name=\"cau$i\" value=\"B\" >". $four_answer[$i]["b"] ."<br>
                            <input type=\"radio\" name=\"cau$i\" value=\"C\" >". $four_answer[$i]["c"] ."<br>
                            <input type=\"radio\" name=\"cau$i\" value=\"D\" >". $four_answer[$i]["d"] ."<br>
                            <br>
                        </div>
                        ";
                    }
                ?>
            </div>

            <div class="btn-submit" id="sm-btn">
                <button>Nạp bài</button>
            </div>


            <div class="btn-continue" id="ct-btn">
                <button type="button">Tiếp</button>
            </div>

            <script>
                var btnSubmit = document.getElementById("sm-btn")
                var btnContinue = document.getElementById("ct-btn")
                btnContinue.onclick = function() {
                    document.getElementById("sm-btn").style.display = "contents"
                    document.getElementById("ct-btn").style.display = "none"
                    document.getElementById("ques1").style.display = "none"
                    document.getElementById("ques2").style.display = "contents"
                }    
            </script>

        </form>
    </div>
</body>

</html>